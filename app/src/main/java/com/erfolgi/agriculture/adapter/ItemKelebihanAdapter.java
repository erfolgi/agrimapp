package com.erfolgi.agriculture.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.activity.DetailCommodityActivity;
import com.erfolgi.agriculture.model.Kategori;
import com.erfolgi.agriculture.model.KategoriData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.erfolgi.agriculture.activity.DetailCommodityActivity.EXTRA_ID;
import static com.erfolgi.agriculture.activity.DetailCommodityActivity.EXTRA_NAMA;

public class ItemKelebihanAdapter extends RecyclerView.Adapter<ItemKelebihanAdapter.ItemKelebihanViewHolder> {
//inisialisasi list untuk model

    //Kurang parameter input model
    private List<KategoriData> listKategoriData;
    private Context context;
    public ItemKelebihanAdapter(List<KategoriData> listKategoriData,Context con){
        this.listKategoriData = listKategoriData;
        this.context=con;
        notifyDataSetChanged();
        Log.d("Debugster", "Constructor");
    }
    public void setData(List<KategoriData> items){
        this.listKategoriData = items;
        notifyDataSetChanged();
    }
    public void updateData(){
        //list add new data from parameter
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemKelebihanViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.d("Debugster", "OnCreateVH");
        return new ItemKelebihanViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list_kelebihan, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemKelebihanViewHolder itemKelebihanViewHolder, int i) {
        //kurang list get position
        Log.d("Debugster", "Adapter");
        itemKelebihanViewHolder.bind(listKategoriData.get(i),context);
    }

    @Override
    public int getItemCount() {
        //kurang list size
        return listKategoriData.size();
    }

    public class ItemKelebihanViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.title_nama_produk)
        TextView title_nama_produk;

        @BindView(R.id.title_lokasi)
        TextView title_lokasi;

        @BindView(R.id.title_jumlah)
        TextView title_jumlah;

        public ItemKelebihanViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        //kurang parameter input model
        public void bind(final KategoriData kategoriData, final Context context){
            //set data to textView
            Log.d("Debugster", "Holder");
            title_nama_produk.setText(kategoriData.getNama());
            title_jumlah.setText(kategoriData.getKuantitas());
            title_lokasi.setText("");
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context , DetailCommodityActivity.class);
                    i.putExtra(EXTRA_ID,kategoriData.getId());
                    i.putExtra(EXTRA_NAMA,kategoriData.getNama());
                    context.startActivity(i);
                }
            });
        }
    }
}
