package com.erfolgi.agriculture.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.api.ApiClient;
import com.erfolgi.agriculture.model.KategoriData;
import com.erfolgi.agriculture.model.KategoriObject;
import com.erfolgi.agriculture.model.PostObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ItemHistoryPostAdapter extends RecyclerView.Adapter<ItemHistoryPostAdapter.ItemHistoryViewHolder> {

    private List<KategoriData> listKategoryData;
    private Context context;

    public ItemHistoryPostAdapter(List<KategoriData> listKategoryData, Context con){
        this.listKategoryData = listKategoryData;
        this.context = con;
        notifyDataSetChanged();
    }
    public void setData(List<KategoriData> items){
        this.listKategoryData = items;
        notifyDataSetChanged();
    }
    public  void updateData(){
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ItemHistoryViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_history_post, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHistoryPostAdapter.ItemHistoryViewHolder itemHistoryViewHolder, int i) {
        itemHistoryViewHolder.bind(listKategoryData.get(i),context);
    }

    @Override
    public int getItemCount() {
        return listKategoryData.size();
    }

    public class ItemHistoryViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.history_title_nama_produk)
        TextView history_title_nama_produk;

        @BindView(R.id.history_title_lokasi)
        TextView history_title_lokasi;

        @BindView(R.id.history_title_jumlah)
        TextView history_title_jumlah;

        @BindView(R.id.btn_history_hapus)
        Button btn_history_hapus;

        @BindView(R.id.btn_history_sunting)
        Button btn_history_sunting;

        public ItemHistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        public void bind(final KategoriData kategoriData, final Context context){
            history_title_nama_produk.setText(kategoriData.getNama());
            history_title_jumlah.setText(kategoriData.getKuantitas());
            btn_history_hapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ApiClient apiClient = new ApiClient();
                    Call<PostObject> apiCallBerita = apiClient.getService().postDataDelete(kategoriData.getId());
                    try {
                        apiCallBerita.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            btn_history_sunting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
