package com.erfolgi.agriculture.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.adapter.ItemKelebihanAdapter;
import com.erfolgi.agriculture.api.APICall;
import com.erfolgi.agriculture.api.ApiClient;
import com.erfolgi.agriculture.model.KategoriData;
import com.erfolgi.agriculture.model.KategoriObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListKelebihan extends Fragment {

    RecyclerView rvListKelebihan;
    List<KategoriData> listKategoriData;
    ApiClient apiClient;
    Context con;
    ItemKelebihanAdapter adp;

    public ListKelebihan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_kelebihan, container, false);
        rvListKelebihan = view.findViewById(R.id.rv_listKelebihan);
        rvListKelebihan.setLayoutManager(new LinearLayoutManager(getContext()));
        apiClient=new ApiClient();
        Call<KategoriObject> apiCall = apiClient.getService().getKomoditasAll();
        con=getContext();
        Log.d("Debugster", "Fragment");
        apiCall.enqueue(new Callback<KategoriObject>() {
            @Override
            public void onResponse(Call<KategoriObject> call, Response<KategoriObject> response) {
                if(response.isSuccessful()){
                    if (response.body() != null){
                        Log.d("Debugster", "onResponse");
                        KategoriObject object = response.body();
                        listKategoriData = object.getData();
                        adp = new ItemKelebihanAdapter(listKategoriData,con);
                        adp.setData(listKategoriData);
                        rvListKelebihan.setAdapter(adp);
                        rvListKelebihan.setLayoutManager(new LinearLayoutManager(getContext()));
                    }
                }
            }

            @Override
            public void onFailure(Call<KategoriObject> call, Throwable t) {
                Toast.makeText(getContext(), "Gagal ambil item bro", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
