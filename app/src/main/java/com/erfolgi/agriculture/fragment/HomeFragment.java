package com.erfolgi.agriculture.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.api.APICall;
import com.erfolgi.agriculture.api.ApiClient;
import com.erfolgi.agriculture.model.KategoriData;
import com.erfolgi.agriculture.model.KategoriObject;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap gMap;
    private SupportMapFragment supportMapFragment;
    private ApiClient apiClient;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        supportMapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.mapView);
        supportMapFragment.getMapAsync(this);


        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        loadDataLokasiKomoditas();
    }

    private void loadDataLokasiKomoditas(){
        Call<KategoriObject> apiCall;
        apiClient = new ApiClient();
        apiCall = apiClient.getService().getKomoditasAll();
        apiCall.enqueue(new Callback<KategoriObject>() {
            @Override
            public void onResponse(Call<KategoriObject> call, Response<KategoriObject> response) {
                if (response.isSuccessful()){
                    List<KategoriData> listData = response.body().getData();
                    initMarker(listData);
                }else{
                    Toast.makeText(getContext(), "Error response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<KategoriObject> call, Throwable t) {
                Toast.makeText(getContext(), "Request failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initMarker(List<KategoriData> listData){
        for(int i=0; i<listData.size(); i++){
            LatLng lokasiKomoditas = new LatLng(Double.parseDouble(listData.get(i).getLatitude()),
                    Double.parseDouble(listData.get(i).getLongitude()));

            gMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(lokasiKomoditas)
                .title(listData.get(i).getNama()));
        }
    }
}
