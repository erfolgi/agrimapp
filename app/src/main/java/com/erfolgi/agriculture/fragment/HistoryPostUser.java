package com.erfolgi.agriculture.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.adapter.ItemHistoryPostAdapter;
import com.erfolgi.agriculture.api.ApiClient;
import com.erfolgi.agriculture.model.KategoriData;
import com.erfolgi.agriculture.model.KategoriObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryPostUser extends Fragment {

    RecyclerView rvHistoryPost;
    List<KategoriData> listKategoryData;
    ApiClient apiClient;
    Context con;
    ItemHistoryPostAdapter adapter;

    public HistoryPostUser() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_history_post_user, container, false);
        rvHistoryPost = view.findViewById(R.id.rv_history_post);
        rvHistoryPost.setLayoutManager(new LinearLayoutManager(con));
        apiClient = new ApiClient();
        Call<KategoriObject> apiCall = apiClient.getService().getKomoditasAll();
        apiCall.enqueue(new Callback<KategoriObject>() {
            @Override
            public void onResponse(Call<KategoriObject> call, Response<KategoriObject> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        KategoriObject object = response.body();
                        listKategoryData = object.getData();
                        adapter = new ItemHistoryPostAdapter(listKategoryData, con);
                        adapter.setData(listKategoryData);
                        rvHistoryPost.setAdapter(adapter);
                        rvHistoryPost.setLayoutManager(new LinearLayoutManager(con));
                    }
                }
            }

            @Override
            public void onFailure(Call<KategoriObject> call, Throwable t) {
                Toast.makeText(con, "Gagal ambil item bro", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}
