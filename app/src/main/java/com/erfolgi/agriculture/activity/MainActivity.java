package com.erfolgi.agriculture.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.fragment.AddOverproduct;
import com.erfolgi.agriculture.fragment.HistoryPostUser;
import com.erfolgi.agriculture.fragment.HomeFragment;
import com.erfolgi.agriculture.fragment.ListKelebihan;

public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.main_container, homeFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_list:
                    ListKelebihan listKelebihan = new ListKelebihan();
                    FragmentTransaction ltransaction = getSupportFragmentManager().beginTransaction();
                    ltransaction.replace(R.id.main_container, listKelebihan);
                    ltransaction.commit();
                    return true;
                case R.id.navigation_add:
                    AddOverproduct add = new AddOverproduct();
                    FragmentTransaction atransaction = getSupportFragmentManager().beginTransaction();
                    atransaction.replace(R.id.main_container, add);
                    atransaction.commit();
                    return true;
                case R.id.navigation_history:
                    HistoryPostUser history = new HistoryPostUser();
                    FragmentTransaction htransaction = getSupportFragmentManager().beginTransaction();
                    htransaction.replace(R.id.main_container, history);
                    htransaction.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_home);
    }

}
