package com.erfolgi.agriculture.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.erfolgi.agriculture.R;
import com.erfolgi.agriculture.api.ApiClient;
import com.erfolgi.agriculture.model.KategoriData;
import com.erfolgi.agriculture.model.KategoriDetail;
import com.erfolgi.agriculture.model.KategoriObject;
import com.erfolgi.agriculture.model.Member;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailCommodityActivity extends AppCompatActivity {
    @BindView(R.id.detail_create) TextView dibuat;
    @BindView(R.id.detail_edit) TextView diedit;
    @BindView(R.id.detail_location) TextView lokasi;
    @BindView(R.id.detail_price) TextView harga;
    @BindView(R.id.detail_gambar) ImageView gambar;
    @BindView(R.id.detail_person) TextView orang;
    @BindView(R.id.btn_email) Button email;
    @BindView(R.id.btn_wa) Button wa;

    public static String EXTRA_ID="id post";
    public static String EXTRA_NAMA="nama post";
    private ApiClient apiClient = new ApiClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_commodity);
        EXTRA_ID=getIntent().getStringExtra(EXTRA_ID);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getIntent().getStringExtra(EXTRA_NAMA));

        ButterKnife.bind(this);

        Call<KategoriDetail> apiCallBerita = apiClient.getService().getKomoditasByID(EXTRA_ID);
        apiCallBerita.enqueue(new Callback<KategoriDetail>() {
            @Override
            public void onResponse(Call<KategoriDetail> call, Response<KategoriDetail> response) {
                if (response.isSuccessful()){
                    if(response.body()!=null){
                        KategoriDetail objek = response.body();
                        Log.d("debugster>", String.valueOf(response));
                        KategoriData data = new KategoriData();
                        data=objek.getData();
                        //getSupportActionBar().setTitle(data.getNama());
                        Log.d("debugster>", String.valueOf(data));
                        Log.d("debugster>", String.valueOf(objek.getData()));
                        dibuat.setText(String.valueOf(data.getCreatedAt()));
                        diedit.setText(data.getUpdatedAt());
                        //LOKASI BELUM
                        //GAMBAR BELUM
                        harga.setText(data.getHarga());
                        final Member mb = data.getMember();
                        orang.setText(mb.getNama());
                        email.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                        "mailto", mb.getEmail(), null));
                                //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
                                startActivity(emailIntent);
                            }
                        });
                        wa.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone="+mb.getNoHp()));
                                startActivity(i);
                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(Call<KategoriDetail> call, Throwable t) {
                Toast.makeText(DetailCommodityActivity.this,"GAGAL MEMUAT: "+t, Toast.LENGTH_SHORT).show();
            }
        });
    }
}

