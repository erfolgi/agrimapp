package com.erfolgi.agriculture.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("lokasi")
    @Expose
    private String lokasi;
    @SerializedName("scan_ktp")
    @Expose
    private String scanKtp;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("onesignal_player_id")
    @Expose
    private String onesignalPlayerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getScanKtp() {
        return scanKtp;
    }

    public void setScanKtp(String scanKtp) {
        this.scanKtp = scanKtp;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOnesignalPlayerId() {
        return onesignalPlayerId;
    }

    public void setOnesignalPlayerId(String onesignalPlayerId) {
        this.onesignalPlayerId = onesignalPlayerId;
    }

}