package com.erfolgi.agriculture.api;

import com.erfolgi.agriculture.model.KategoriDetail;
import com.erfolgi.agriculture.model.KategoriObject;
import com.erfolgi.agriculture.model.PostObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APICall {
    //komoditas/   //Gatau bener atau engga :D ----> masih statis kayaknya
    @GET("komoditas")
    Call<KategoriObject> getKomoditasAll();
    @GET("komoditas/{id}/id")
    Call<KategoriDetail> getKomoditasByID(@Path("id") String id);
    @GET("komoditas/{id}/kategori")
    Call<KategoriObject> getKomoditasByKategori(@Path("id") String id);
    @GET("komoditas/{id}/member")
    Call<KategoriObject> getKomoditasByMember(@Path("id") String id);

    //POST
    @POST("komoditas/add")
    @FormUrlEncoded
    Call<PostObject> postDataBaru(@Field("kategori_id") String kategori_id,
                                  @Field("member_id") String member_id,
                                  @Field("nama") String nama,
                                  @Field("kuantitas") String kuantitas,
                                  @Field("harga") String harga,
                                  @Field("latitude") String latitude,
                                  @Field("longitude") String longitude);
    @POST("komoditas/edit")
    @FormUrlEncoded
    Call<PostObject> postDataEdit(@Field("kategori_id") String kategori_id,
                                  @Field("member_id") String member_id,
                                  @Field("nama") String nama,
                                  @Field("kuantitas") String kuantitas,
                                  @Field("harga") String harga,
                                  @Field("latitude") String latitude,
                                  @Field("longitude") String longitude,
                                  @Field("id") String id);
    @POST("komoditas/delete")
    @FormUrlEncoded
    Call<PostObject> postDataDelete(@Field("id") String id);

    //Account
    @POST("member/login")
    @FormUrlEncoded
    Call<PostObject> postMemberLogin(@Field("email") String email,
                                     @Field("password") String password);
    @POST("member/new")
    @FormUrlEncoded
    Call<PostObject> postMemberNew(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("nama") String nama,
                                   @Field("alamat") String alamat,
                                   @Field("lokasi") String lokasi,
                                   @Field("no_hp") String no_hp,
                                   @Field("type") String type);
}
